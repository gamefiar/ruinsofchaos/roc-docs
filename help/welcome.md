<div class="th" id="top">Welcome</div>
<div class="td">
    Ruins of Chaos is a role-playing game. The role you play is in an
    interactive war. Your skill and determination are put up against
    that of <s>thousands</s> dozens of other players. You define your
    own success. You define your own war tactics. Build an army and
    execute missions of war, training, building, or trade in a series of
    web pages that work in just about every browser -- even a phone.<br />
    <br /> The game separates the play into periods of time called ages,
    which typically last 2-4 months. On each new age, everyone starts
    over anew to prove their worth. The culmination of that effort is
    recorded in the ever-building history books of the game at the
    closing of each age.<br /> <br /> The objectives of the game within
    each age depend on what part of the game you enjoy the most. There
    are top ranks recorded at the end of each age in many areas, and
    your overall rank at any time is based on how well-balanced an army
    you have. But there is much more fun to be had than just numbers.
    The game is a society -- much more than what you can see on this
    website. People bring their friends along to play with them and
    compete with other teams of players. They test the limits of their
    competitors, sometimes making enemies out of them. They fight their
    enemies with their friends, sometimes ending up becoming better
    friends with their enemies afterwards, and the cycle starts all over
    again.<br /> <br /> Gathering friends is one of the simplest ways to
    advance, yet hardest to master. Friends can build command chains
    together to work toward a common goal. The more power you have under
    your command chain, the bigger your army will grow, and the more
    power and influence you'll have in the game. The more influence you
    have, the greater the chance will be that one of your friends will
    choose to sacrifice themselves to make your army the greatest.
</div>
