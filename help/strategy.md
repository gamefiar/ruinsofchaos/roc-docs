<div class="th" id="strategy">Other Common Strategies</div>
<div class="td">
    There are many ways to further differentiate the gameplay depending
    on your preferences. Many of them are common to all basic styles.
    <ul>
        <li><b>Training/Untraining</b>
            <ul>
                <li>Trained soldiers are <?=((($g->soldier['attack_soldiers']->strength - $g->soldier['untrained_soldiers']->strength) / $g->soldier['untrained_soldiers']->strength) * 100)?>% more effective at their specialized area than untrained soldiers, but they can ONLY hold the type of weapon they specialize in. Untrained soldiers can fight with EITHER an attack or a defense weapon, so they affect both of those areas of your army's stats at the same time.</li>
                <li>Trained soldiers are harder to kill than untrained soldiers.
                    See more below in <b>Casualties</b>.
                </li>
                <li>Therefore, if you have an excess of soldiers, training will
                    maximize your power by enabling you to use your weapons at their
                    strongest.</li>
                <li>On the other hand, if you have many more weapons than you
                    have soldiers, untraining all of your soldiers will allow you to
                    pick up one of each in attack and defense, thus optimizing your
                    power for a limited number of soldiers.</li>
            </ul></li>
        <li><b>Mercenaries</b>
            <ul>
                <li>Mercenaries can be hired as cannon fodder that will shield your valuable primary (gold-producing) troops against casualties. They're easy to hire, all they ask is <?=number_format($g->soldier['attack_mercs']->cost)?> Gold and free access to your pizza and beer. Of course, since their primary function in battle is to suffer the casualties, one wonders how they're going to spend the gold you pay them. But that's their problem, not yours.</li>
                <li>A limited number of mercenaries is available at any given
                    time. Each turn, a number of mercenaries make themselves
                    available for purchase in the Training section, and it is a race
                    to buy up the mercs. If you are not quick enough, you may not be
                    able to snag up enough to cover your losses. Plan ahead, and buy
                    mercenaries as soon as you can afford them!</li>
            </ul></li>
        <li><b>Weapon Cost Effectiveness</b>
            <ul>
                <li><b>Cheaper weapons are more cost effective</b>. The trade-off
                    is that you need more soldiers to hold those cheaper weapons,
                    putting more soldiers at risk of death, and reducing your
                    training flexibility.</li>
            </ul></li>
        <li><b>Casualties</b>
            <ul>
                <li>During battle, your fighting force will inevitably suffer
                    casualties. Many factors affect the amount of casualties:
                    strength of the attack or defense in relation to the enemy, size
                    of the fighting force (total attackers or total defenders), and
                    weapon armaments.</li>
                <li>The most effective way to reduce casualties is by training
                    unneeded soldiers to the side which incurs the fewest battles:
                    if you are a ranker or banker, train them to attack so that the
                    minimum amount of troops are on defense, thus minimizing losses
                    when you get attacked. If you are a slayer, train them to
                    defense. In either case, leave enough to hold all of your
                    weapons.</li>
                <li>Further reduction of casualties may be possible if you avoid
                    having untrained soldiers (those that you receive from clicks,
                    unit production, and officer bonuses). An untrained army will
                    suffer roughly twice as many casualties as a trained army of the
                    same size. If at least 75% of your soldiers in an attack or
                    defense are trained soldiers, your casualties will be greatly
                    reduced. Mercs do not count toward your trained or untrained
                    total in this case, as mercs decide to get killed at a rate of
                    their own choosing, since they're the ones that signed up for
                    this job.</li>
            </ul></li>
        <li><b>Covert Operations</b>
            <ul>
                <li>Upgrading your Covert Skill as soon as possible is a good way
                    to increase your effectiveness in executing covert operations,
                    as well as intercepting operations against your army.</li>
                <li>Spies and sentries will die if you are unable to defend an
                    attack against your army. Reducing spy/sentry casualties is only
                    possible by minimizing the number of spies and sentries you
                    train and increasing your defense to evade attackers.</li>
                <li>Sometimes, sabotaging your aggressors (or the threat itself)
                    is more effective than diplomacy or simply outgrowing their
                    onslaught. Learn to use the sabotage mission to your advantage.
                    However, sabotage is the most hostile action in the game, and
                    can sometimes start a deadly war. Use it wisely, or be prepared
                    to lose everything.</li>
            </ul></li>
    </ul>
</div>
