<div class="th" id="battle">Battle</div>
<div class="td">
    Battle is at the core of the game. Attacking, defending, reconning,
    sabotaging, and protecting yourself from reconning and sabotaging
    are what it's all about.
    <ul>
        <li><b>Military Divisions, a.k.a. Stats</b>
            <ul>
                <li>Your army's strength and rank in each of four areas is based
                    on a computed Senseless and Totally Arbitrary Tactical Strength
                    number, which is determined by a formula that takes soldiers,
                    weapons, upgrades, and bonuses into account. These calculated
                    numbers are referred to as <b>stats</b>, and are the basic unit
                    of comparing your army to another's.
                </li>
                <li><b>Strike</b>, also known as <b>Strike Action</b>, or <b>SA</b>
                    for short, is the combined power of your available attack force
                    (trained soldiers, trained mercenaries, untrained soldiers, and
                    untrained mercenaries), armed with the attack weapons you have
                    available, and multiplied by the strength of your siege
                    technology.</li>
                <li><b>Defense</b> (<b>Defensive Action</b>, or <b>DA</b>) is the
                    combined power of your available defense force, armed with the
                    defense weapons you have available, and multiplied by the
                    strength of your fortification. DA is the countermeasure against
                    an enemy's SA.</li>
                <li><b>Spy</b> (<b>Spy Rating</b>, or <b>SP</b>) is your
                    effectiveness at covert operations, determined by the power of
                    your spies, armed with spy weaponry, multiplied by the strength
                    of your covert skill upgrade.</li>
                <li><b>Sentry</b> (<b>Sentry Rating</b>, or <b>SE</b>) is your
                    effectiveness at stopping covert operations, determined by the
                    power of your sentries, armed with sentry weaponry, and also
                    multiplied by the strength of your covert skill upgrade.</li>
            </ul> <br /> <br /> <br /> <br /></li>
        <li><b>Total Fighting Force:</b> (TFF) A measure of your army size,
            comprised of all trained and untrained soldiers and mercenaries.
            Not including spies and sentries, as these are covert operatives.
            TFF is a factor in attack missions, so keep it in mind as you
            continue reading.</li>
        <li><b>Turn-Based Gold:</b> (TBG) A measure of your income, derived
            from the amount of soldiers you have, and to a lesser extent, the
            amount of covert operatives you have. Mercenaries are hired, and
            as such do not contribute to your TBG. Your TBG is a main factor
            in two things: how fast you can build your own stats, and how
            attractive a target you become to the slayers. Build up too much
            gold without spending, and you're in somebody's crosshairs. <br />
            <br /> <br /> <br /></li>
        <li><b>Attack Mission</b>
            <ul>
                <li>Attacking another army causes your Strike division to contend
                    with the enemy's Defense division. Combined with random factors
                    in both armies' actions during the battle, the comparison
                    between strike damage and defense damage will decide the victor.</li>
                <li>Victory for you means that you steal some of the enemy's
                    gold, also called <b>pillage</b>, or a <b>hit</b>. The amount of gold you steal depends on two factors: the amount of attack turns you used (out of <?=$g->data['maxturns']?>), and the comparison of your total fighting force with the enemy's total fighting force.</li>
                <li>The higher your total fighting force in comparison with your
                    enemy's, the less gold you will be able to steal. This is in
                    order to balance the game out so that the largest players cannot
                    pick on the smallest players, thereby gaining a double advantage
                    -- earning more TBG as well as being able to attack everyone
                    below them for even more gold.</li>
                <li>During the course of an attack, your weapons could take
                    damage, and your troops could take casualties. This can cause
                    you to lose stats over time, unless you repair the damage and
                    replenish troops by recruiting, or supplementing them with
                    mercenaries.</li>
                <li>Failing to overcome the enemy's defense during a particular
                    attack will cause them to win, and lose no gold, although damage
                    and casualties will still occur. This is called being defended.</li>
                <li>Being defended, failing to take enough gold, or taking gold
                    too often from an enemy may incite retaliation by sabotage. Read
                    the enemy's profile (sometimes referred to as <b>AA</b> by other
                    players) in order to understand their rules of engagement. It is
                    up to your own judgment as to whether your enemy's rules of
                    engagement are reasonable; don't
                </li>
            </ul> <br /> <br /> <br /> <br /></li>
        <li><b>Recon Mission</b>
            <ul>
                <li>A recon mission allows you to sneak into an enemy's base to
                    learn more about their stats, upgrades, and weapons. You send a
                    variable amount of spies, based on how many spies you want to
                    risk and how much data you want to gather should your mission be
                    a success. Sending only one spy comes with the least risk, as it
                    is rare that a lone spy will die, even if the enemy detects the
                    attempt. However, depending on the comparison between your spy
                    rating and the target's sentry rating, your spy may not be able
                    to gather enough data before being forced to make an escape.
                    Thus, sending more spies will allow you to gather more data at a
                    time.</li>
                <li>The intelligence report gathered during a recon mission will
                    show you various details about your target. Some information
                    will show up in red if it defeats your counterpart. For
                    instance, if the enemy's defense is higher than your strike, the
                    defense section of the report will be colored in red. This will
                    give you a quick glance at whether you can defeat them.</li>
                <li>One of the most important parts of a recon mission can be the
                    current amount of gold the target is holding. This is an item
                    which you may be able to see immediately from the battlefield,
                    if your spy is higher than the target's sentry. Otherwise, you
                    must recon to find it, or seek assistance from an alliance
                    member or friend in order to know.</li>
            </ul> <br /> <br /> <br /> <br /></li>
        <li><b>Sabotage Mission</b>
            <ul>
                <li>The sabotage mission is the deadliest mission in the game.
                    Sabotage allows you to destroy part of the enemy's armory by
                    sending spies on a mission to target their weapons. Sabotage is
                    most often the catalyst for escalated hostilities, as well as
                    being the most common tactic used for an all-out war.</li>
                <li>Sneaking into an armory to sabotage weapons is much more
                    difficult than a recon mission. The likelihood that your spies
                    can enter the armory undetected depends largely on the
                    comparison between your spy and the target's sentry. Basically,
                    the bigger, the better.</li>
                <li>In large part, the amount of weaponry you can sabotage
                    depends on how many spies you send, your covert skill level,
                    your total armory value, and the target's armory value. The more
                    you and your target have, the more of your target's weaponry you
                    can destroy.</li>
                <li>Weapons will be completely destroyed if they are sabotaged
                    and used. The enemy does not find out the weapons are sabotaged
                    until they try to use them and <b>break</b> them (i.e. by
                    attacking and using attack weapons, or by getting attacked,
                    using defense weapons).
                </li>
                <li>Prior to breaking weapons, they can be <b>sold</b> in order
                    to prevent breaking them and taking a bigger loss. Selling
                    weapons will magically take sabotaged weapons off of the top of
                    the pile and give you full resale value. Sucks for the poor sap
                    who buys them off of you, eh? ;)
                </li>
                <li>Being caught attempting a sabotage mission, which is quite
                    common, will alert your target at what you are attempting to do.</li>
                <li>If a sabotage mission is successful, <b>it will not show up
                        in your enemy's intel logs</b>. So, they will only find out you
                    are sabotaging them if you fail an attempt. You may attempt up
                    to 10 times per 22 hours.
                </li>
            </ul></li>
        <br />
        <br />
        <br />
        <br />
        <li><b>Upgrades</b>
            <ul>
                <li>Upgrading your army's technology is a way to advance your
                    army's effectiveness and productivity.</li>
                <li>All upgrades increase in price at a faster rate than their
                    increase in strength. For every upgrade, there is a point at
                    which, instead of continuing to buy weapons, you can buy the
                    upgrade and increase your stats more.</li>
                <li>Attack and defense upgrades can be purchased in the armory.
                    They are called Siege and Fortification (or Fort for short),
                    respectively.</li>
                <li>Covert skill and unit production upgrades can be purchased in
                    training.
                    <ul>
                        <li>Covert skill increases both spy and sentry, as well as how
                            much damage you can cause in sabotage.</li>
                        <li>Unit production provides you with soldiers at a steady
                            rate: every 6 hours, your army will automatically recruit a
                            quarter (1/4) of your unit production's total daily soldier
                            intake. Additionally, when you buy a unit production upgrade,
                            you instantly get a full 24 hour's worth of soldiers from the
                            new upgrade.</li>
                    </ul>
                </li>
                <li>You will see your target's fort level when you view them from
                    afar, on their stats page. Generally, you can get a good idea of
                    how advanced your potential target's defense is by the number of
                    their fort level. A recon mission will reveal more information
                    about the target's defense.</li>
            </ul></li>
    </ul>
</div>
