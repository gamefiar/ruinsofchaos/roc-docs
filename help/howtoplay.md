<div class="th" id="howtoplay">How do I play?</div>
<div class="td">
    Play from your web browser or your phone. Control your army by
    loading up a page and filling in the numbers. Simple as that.<br />
    <br /> The basic economy is simple, with the following core elements
    making up the bulk of the gameplay:
    <ul>
        <li><b>Turn</b>: The unit of time. Every 30 minutes, another turn
            passes.</li>
        <li><b>Attack Turn</b>: The basic unit of attacking. One is given
            to you every 30 minutes. You use attack turns to probe or attack
            other players.</li>
        <li><b>Gold</b>: The only monetary unit in the game. Gold is made
            every 30 minutes whether you are online or not. Gold can be stolen
            from you at any time. Use gold to increase your defenses to try to
            stop losing your gold, or increase your offensive power to try to
            take other people's gold.</li>
        <li><b>Soldier</b>: The fundamental infantry unit. Soldiers make
            gold for your army (or someone else, if you don't spend it), and
            are used in battle.</li>
        <li><b>Credit</b>: A credit is a token you earn which is good for
            one soldier. Credits can be earned by spending time in the recruit
            center. You can either use them for yourself to get more soldiers,
            or send them to someone else as payment for deeds done.</li>
        <li><b>Weapon</b>: Weapons are used by soldiers to perform their
            particular actions: attacking, defending, spying, sabotaging, or
            intercepting covert missions. Weapons are a necessity, as the bare
            unarmed soldiers can't do much.</li>
    </ul>
    Your army's actions are divided into a few main areas:
    <ul>
        <li><b>Recruiter</b>: Use this to earn soldiers. By default, your
            soldiers are enlisted immediately into your army, but you can save
            soldiers as credits to use or send later.</li>
        <li><b>Armory</b>: Buy weapons here. Weapons are what your soldiers
            need in order to be effective in battle.</li>
        <li><b>Training</b>: Train soldiers to make them more effective.
            Protect your recruited soldiers from dying by hiring mercenaries,
            who will then become your front line of offense and defense.</li>
        <li><b>Buildings</b>: Buy upgrades for your army to increase the
            effectiveness of each soldier and weapon.</li>

        <li><b>Battlefield</b>: Scout the land to search for gold to
            pillage. From the Battlefield, click on a name to do one of the
            following:
            <ul>
                <li><b>Attack</b>: Travel to another player's base and attempt to
                    beat their defense and steal their gold. Uses 12 turns.</li>
                <li><b>Recon</b>: Send a covert operative to sneak through
                    somebody's base and find out their stats, so that you will know
                    if you can beat them.</li>
                <li><b>Sabotage</b>: Destroy some of your target's weaponry. If a
                    player is stepping over your boundaries and all other avenues of
                    communication have failed, sabotage is one way to get your point
                    across. Sometimes the best defense is a good offense!</li>
                <li><b>Probe</b>: Same as an attack but uses one turn, doesn't
                    steal as much gold. Mostly used to probe the player's defenses
                    if you can't recon, or to damage their weapons and kill their
                    soldiers during war.</li>
            </ul></li>

        <li><b>Alliances</b>: Seek out help from a group of players that
            suits you, or forge your own alliance, get your friends to join,
            and lead them all to greatness.</li>
    </ul>
</div>
