<div class="th" id="recruiter">Recruiter</div>
<div class="td">
    Every army worth its weight in pizza and beer needs soldiers! You
    start off with some and instantly gain more every minute. Your
    recruiting campaign will slow down and not earn as many soldiers
    after a period of time. Complete the CAPTCHA to renew your
    recruiting campaign and continue earning as many soldiers as
    possible.<br> <br> Soldiers that your officers recruit will
    automatically bear some little bonus soldiers for you as well! (1/4
    as many)
</div>
