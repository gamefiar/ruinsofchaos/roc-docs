<table class="sep" cellspacing="0" id="settings">
    <tr>
        <th colspan="2">Game Variables</th>
    </tr>
    <tr>
        <td>Minutes between turns</td>
        <td class="r">30</td>
    </tr>
    <tr>
        <td>Base Income (TBG)</td>
        <td class="r">
45 per Attack Soldier
-10 per Attack Mercenary
60 per Defense Soldier
-10 per Defense Mercenary
45 per Untrained Soldier
-5 per Untrained Mercenary
20 per Spy
20 per Sentry
        </td>
    </tr>
    <tr>
        <td>Mercenary Limit</td>
        <td class="r">25% of your total fighting force<br /> (i.e. 1/3rd as
            many mercs as total paying soldiers)
        </td>
    </tr>
    <tr>
        <td>Mercenary Distribution</td>
        <td class="r">(x^0.6) of each kind per turn, where x is the total
            amount of soldiers among all active players (x^0.5) of each kind
            at the top of the minute if mercenaries are depleted between turns</td>
    </tr>
    <tr>
        <td>What comes every turn</td>
        <td class="r">Income, 1 Attack Turn</td>
    </tr>
    <tr>
        <td>Attack Turns Needed for Full Attack</td>
        <td class="r">12 turns (except during Double Trouble)</td>
    </tr>
    <tr>
        <td>Attacks allowed per target</td>
        <td class="r">5 per 24 hours</td>
    </tr>
    <tr>
        <td>Recon missions per target</td>
        <td class="r">10 per 24 hours</td>
    </tr>
    <tr>
        <td>Sabotage missions per target</td>
        <td class="r">10 per 22 hours</td>
    </tr>
    <tr>
        <td>Number of spies able to be used</td>
        <td class="r">25 per mission</td>
    </tr>
    <tr>
        <td>Base Strength of Untrained Troops</td>
        <td class="r"><?=$g->soldier['untrained_soldiers']->strength?> * Siege or Fortification Multiplier</td>
    </tr>
    <tr>
        <td>Base Strength of Trained Troops</td>
        <td class="r"><?=$g->soldier['attack_soldiers']->strength?> * Siege or Fortification Multiplier</td>
    </tr>
    <tr>
        <td>Base Strength of Covert Troops</td>
        <td class="r">Covert Skill Level</td>
    </tr>
    <tr>
        <td>Click Trickle</td>
        <td class="r">25% to each commander, up to the 7th commander</td>
    </tr>
    <tr>
        <td>Names and Strengths of Upgrades</td>
        <td class="l">
            <table id="upgrades" class="sep">
                <tr>
                    <th colspan="5">Attack</th>
                </tr>
                <tr>
                    <th class="subh r">#</th>
                    <th class="subh">Name</th>
                    <th class="subh r">Cost</th>
                    <th class="subh r">Strength</th>
                    <th class="subh r">Bonus point?</th>
                </tr>
<?php

	foreach ($g->siege as $index => $upgrade)
	{
		?>
                <tr>
                    <td class="r"><?=$index?></td>
                    <td><?=$upgrade->name?></td>
                    <td class="r"><?=number_format($upgrade->cost)?></td>
                    <td class="r"><?=$upgrade->strength?></td>
                    <td class="r"><?=($upgrade->bonuspoints ? $upgrade->bonuspoints : '&nbsp;')?></td>
                </tr>
                <?php
	}
	?>
                <tr>
                    <th colspan="5">Defense</th>
                </tr>
                <tr>
                    <th class="subh r">#</th>
                    <th class="subh">Name</th>
                    <th class="subh r">Cost</th>
                    <th class="subh r">Strength</th>
                    <th class="subh r">Bonus point?</th>
                </tr>
<?php

	foreach ($g->fort as $index => $upgrade)
	{
		?>
                <tr>
                    <td class="r"><?=$index?></td>
                    <td><?=$upgrade->name?></td>
                    <td class="r"><?=number_format($upgrade->cost)?></td>
                    <td class="r"><?=$upgrade->strength?></td>
                    <td class="r"><?=($upgrade->bonuspoints ? $upgrade->bonuspoints : '&nbsp;')?></td>
                </tr>
<?php
	}
	?>
                <tr>
                    <th colspan="5">Covert</th>
                </tr>
                <tr>
                    <th class="subh r">#</th>
                    <th class="subh">Name</th>
                    <th class="subh r">Cost</th>
                    <th class="subh r">Strength</th>
                    <th class="subh r">Bonus point?</th>
                </tr>
<?php

	foreach ($g->skill as $index => $upgrade)
	{
		?>
                <tr>
                    <td class="r"><?=$index?></td>
                    <td><?=$upgrade->name?></td>
                    <td class="r"><?=number_format($upgrade->cost)?></td>
                    <td class="r"><?=$upgrade->strength?></td>
                    <td class="r"><?=($upgrade->bonuspoints ? $upgrade->bonuspoints : '&nbsp;')?></td>
                </tr>
<?php
	}
	?>
            </table>
        </td>
    </tr>
    <tr>
        <td>Online Indicator Timeout</td>
        <td class="r">24 minutes</td>
    </tr>
    <tr>
        <td>What does the scouter say about his power level?</td>
        <td class="r">It's over NINE THOUSAAAAAAAAAND</td>
    </tr>
    <tr>
        <td>Login Timeout</td>
        <td class="r">3 hours</td>
    </tr>
</table>
