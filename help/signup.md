<div class="th" id="howtosignup">How do I sign up?</div>
<div class="td">
    <a href="register.php" target="_new">Register</a> your account by
    filling out the few boxes on that page, and finish by clicking the
    number. It really is as simple as that!<br /> <br /> Once signing
    up, you may reset your account to choose another race once every 24
    hours (except in the hours prior to the start of an age, where you
    can reset indefinitely until the start).<br /> <br /> After signing
    up, you will need to verify your account within 3 days by clicking
    on a link that gets emailed to the address you signed up with. Since
    your email is used as your login ID, it is important to use one that
    is secure and working. We recommend against using free services like
    Hotmail, which may mistake our verification emails for spam.<br /> <br />
    If you didn't get the email in a reasonable amount of time, say, 30
    minutes, check your junk mail folder or <a href="prefs.php">select a
        new email</a>.
</div>
<div class="th" id="races">Races</div>
<div class="td">There are <?=(sizeof($g->race) - 1)?> races in Ruins of Chaos:</div>

<?php include ('../races.php')?>

<div class="th" id="style">Playing Style</div>
<div class="td">
    There are a few different kinds of gameplay strategies, and your
    chosen race's bonus may help or hurt each strategy.<br /> <br />
    <ul>
        <li><b>Ranker</b>
            <ul>
                <li>Prefers gaining as many soldiers as possible and trying to
                    get the best rank</li>
                <li>Usually the most well-balanced armies, while not being overly
                    powerful in any one area</li>
            </ul></li>
        <li><b>Banker</b>
            <ul>
                <li>Prefers keeping gold above all else</li>
                <li>Spends gold several times a day</li>
                <li>Usually hides in the rankings, typically with a highly
                    defensive build</li>
            </ul></li>
        <li><b>Spy/Sabber</b>
            <ul>
                <li>Prefers being able to find more gold and sabotage more
                    effectively</li>
                <li>Spends gold mostly toward repairing weapons in war</li>
                <li>Usually hides in the rankings with high covert stats</li>
            </ul></li>
        <li><b>Slayer</b>
            <ul>
                <li>Prefers taking all the gold they can</li>
                <li>Spends their own gold a few times a day; spends other
                    people's gold as often as they damn well please!</li>
                <li>Typically focuses on attack, and possibly spy if they don't
                    have any spy friends</li>
            </ul></li>
    </ul>
    <br /> <br /> There are, of course, some exceptions to these trends,
    but they can vary from age to age depending on what's different
    about the gameplay settings and who decides to play.
</div>
