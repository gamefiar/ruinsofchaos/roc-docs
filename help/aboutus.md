<table class="sep" cellspacing="0" id="aboutus">
<div class="th" id="aboutus">About Us</div>
<p>Ruins of Chaos is an exciting online text-based game which was
    designed to help you unleash your inner Orc, Elf, Dwarf, or,
    perhaps, the Human in you! Maybe even the Pixie, if you're feeling
    fairy adventurous.</p>

<p>Enjoy the chaos while we rewrite this page!</p>
