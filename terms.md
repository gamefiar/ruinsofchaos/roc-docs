# Terms of Service ("Terms")
Last updated: March 10, 2018

## Scope

Please read these Terms of Service ("Terms", "Terms of Service")
carefully before using the Ruins of Chaos website at $_SERVER[SERVER_NAME]
("Service", "Game") operated by gamefiar, LLC ("us", "we", or "our").

Your access to and use of the Service is conditioned upon your
acceptance of and compliance with these Terms. These Terms apply
to all visitors, users and others who wish to access or use the
Service.

**By accessing or using the Service you agree to be bound by
these Terms. If you disagree with any part of the terms
then you do not have permission to access the Service.**

## Account Information

To use the Service, you must create an Account that includes an
email address (used to log in or recover your login), a username
(which you will be referred to as within the Game) and a password.

Upon creating an Account, you agree to the following:

{type="A"}
1.  You are solely responsible for any information associated with
	your Account and for anything that happens related to your Account.
2.  You do not already have an Account.
3.  You will not sell or gift your Account or access to it to any
	other individual. Accounts may be transferred between players with
	prior permission from Gamefiar staff.
4.  You do not have and will never have ownership or any other
	property interest in any Account. You further acknowledge and agree
	that all right in and to such Accounts are and will always be owned
	by gamefiar, LLC.

When you create an account with us, you guarantee that you are above
the age of 18, and that the information you provide us is accurate,
complete, and current at all times. Inaccurate, incomplete, or
obsolete information may result in the immediate termination of your
account on the Service.

You are responsible for maintaining the confidentiality of your
account and password, including but not limited to the restriction of
access to your computer and/or account. You agree to accept
responsibility for any and all activities or actions that occur under
your account. You must notify us immediately upon becoming aware of
any breach of security or unauthorized use of your account.

We reserve the right to refuse service, terminate accounts, remove
or edit content, or cancel orders in our sole discretion.

## Code of Conduct

While using the Service, you agree to comply with all applicable
laws, rules and regulations. You also agree to comply with the
additional rules that govern your use of the Service (the "Code of
Conduct"). The Code of Conduct is not meant to be exhaustive, and
gamefiar, LLC reserves the right to modify the Code of Conduct at any
time and to take any disciplinary actions including Account
termination and suspension to protect the integrity and spirit of the
Code of Conduct, regardless of whether a specific behavior is listed
here as prohibited. The following list provides examples of behavior
that warrants disciplinary action:

{type="A"}
1.  Impersonating any real person, regardless of who the person may be
2.  Posting identifying information about yourself, or any other user,
	within the Game
3.  Transmitting or communicating any content or language which, in
	the sole judgment of gamefiar, LLC, is deemed to be offensive. This
	includes without limitation content or language that is illegal,
	vulgar, aggressive, insulting, disparaging, defamatory, crude, lewd,
	hateful, sexually explicit, or racially, ethnically, nationally, or
	otherwise objectionable. Any and all purposeful misspellings of such
	content to avoid detection or otherwise circumvent restrictions shall
	considered to be in violation of the Code of Conduct
4.  Transmitting or facilitating the transmission of any content that
	contains a virus, corrupted data, trojan horse, bot, keystroke
	logger, or other computer programming routines that are intended to
	and/or actually damage, detrimentally interfere with, surreptitiously
	intercept or mine, scrape or expropriate any system, data or personal
	information
5.  Perform any act that hinders other Users' ability to use the Game
	properly, including, but not limited to spamming Chat.
6.  Using any unauthorized third party programs, including but not
	limited to "scripts," "bots," and other programs which automate any
	portion of the Game.

## Purchases

If you wish to purchase any product or service made available
through the Service ("Purchase"), you may be asked to supply certain
information relevant to your Purchase including, without limitation,
your credit card number, the expiration date of your credit card, your
billing address, and your shipping information.

You represent and warrant that:

{type="i"}
1.  you have the legal right to use
	any credit card(s) or other payment method(s) in connection with any
	Purchase; and that
2.  the information you supply to us is true, correct and complete.

The service may employ the use of third party services for the
purpose of facilitating payment and the completion of Purchases. By
submitting your information, you grant us the right to provide the
information to these third parties subject to our Privacy Policy.

We reserve the right to refuse or cancel your order at any time for
reasons including but not limited to: product or service availability,
errors in the description or price of the product or service, error in
your order or other reasons.

We reserve the right to refuse or cancel your order if fraud or an
unauthorized or illegal transaction is suspected.

gamefiar, LLC may revise the pricing of its Products at any time.
All fees and charges are payable in accordance with payment terms in
effect at the time the fee or the charge becomes due and payable.

All sales are final. Due to the nature of digital goods, your
purchases will be delivered immediately upon receipt of your payment.
No refunds will be permitted.

If there is a dispute regarding payment of fees to gamefiar, LLC,
your Account may be closed without warning or notice at the sole
discretion of gamefiar, LLC until such fees have been paid in full.

## Availability, Errors and Inaccuracies

We are constantly updating product and service offerings on the
Service. We may experience delays in updating information on the
Service and in our advertising on other web sites. The information
found on the Service may contain errors or inaccuracies and may not be
complete or current. Products or services may be mispriced, described
inaccurately, or unavailable on the Service and we cannot guarantee
the accuracy or completeness of any information found on the Service.

We therefore reserve the right to change or update information and
to correct errors, inaccuracies, or omissions at any time without
prior notice.

## Intellectual Property

The Service and its original content (excluding Content provided by
users), features and functionality are and will remain the exclusive
property of gamefiar, LLC and its licensors. The Service is protected
by copyright, trademark, and other laws of both the United States and
foreign countries. Our trademarks and trade dress may not be used in
connection with any product or service without the prior written
consent of gamefiar, LLC.

## Links To Other Web Sites

Our Service may contain links to third party web sites or services
that are not owned or controlled by gamefiar, LLC.

gamefiar, LLC has no control over, and assumes no responsibility for
the content, privacy policies, or practices of any third party web
sites or services. We do not warrant the offerings of any of these
entities/individuals or their websites.

You acknowledge and agree that gamefiar, LLC shall not be
responsible or liable, directly or indirectly, for any damage or loss
caused or alleged to be caused by or in connection with use of or
reliance on any such content, goods or services available on or
through any such third party web sites or services.

We strongly advise you to read the terms and conditions and privacy
policies of any third party web sites or services that you visit.

## Content

Our Service allows you to post, link, store, share and otherwise
make available certain information, text, graphics, videos, or other
material ("Content"). You are responsible for the Content that you
post on or through the Service, including its legality, reliability,
and appropriateness.

By posting Content on or through the Service, You represent and
warrant that: 

{type="i"}
1.  the Content is yours (you own it) and/or you have the right to
	use it and the right to grant us the rights and license as
	provided in these Terms, and
2.  that the posting of your Content on or through the Service
	does not violate the privacy rights, publicity rights,
	copyrights, contract rights or any other rights of any person
	or entity.

We reserve the right to terminate the account of anyone found to be
infringing on a copyright.

You retain any and all of your rights to any Content you submit,
post or display on or through the Service and you are responsible for
protecting those rights. We take no responsibility and assume no
liability for Content you or any third party posts on or through the
Service. However, by posting Content using the Service you grant us
the right and license to use, modify, publicly perform, publicly
display, reproduce, and distribute such Content on and through the
Service. You agree that this license includes the right for us to make
your Content available to other users of the Service, who may also use
your Content subject to these Terms.

gamefiar, LLC has the right but not the obligation to monitor and
edit all Content provided by users.

In addition, Content found on or through this Service are the
property of gamefiar, LLC or used with permission. You may not
distribute, modify, transmit, reuse, download, repost, copy, or use
said Content, whether in whole or in part, for commercial purposes or
for personal gain, without express advance written permission from us.

## Termination

We may terminate or suspend your account and bar access to the
Service immediately, without prior notice or liability, under our sole
discretion, for any reason whatsoever and without limitation,
including but not limited to a breach of the Terms.

If you wish to terminate your account, you may simply discontinue
using the Service.

All provisions of the Terms which by their nature should survive
termination shall survive termination, including, without limitation,
ownership provisions, warranty disclaimers, indemnity and limitations
of liability.

## Indemnification

You agree to defend, indemnify and hold harmless gamefiar, LLC and
its licensee and licensors, and their employees, contractors, agents,
officers and directors, from and against any and all claims, damages,
obligations, losses, liabilities, costs or debt, and expenses
(including but not limited to attorney's fees), resulting from or
arising out of

{type="A"}
1.  your use and access of the Service, by you or any person using
	your account and password;
2.  a breach of these Terms, or
3.  Content posted on the Service.

## Limitation Of Liability

In no event shall gamefiar, LLC, nor its directors, employees,
partners, agents, suppliers, or affiliates, be liable for any
indirect, incidental, special, consequential or punitive damages,
including without limitation, loss of profits, data, use, goodwill, or
other intangible losses, resulting from

{type="i"}
1.  your access to or use of or inability to access or use the Service;
2.  any conduct or content of any third party on the Service;
3.  any content obtained from the Service; and
4.  unauthorized access, use or alteration of your transmissions or content,
	whether based on warranty, contract, tort (including negligence) or
	any other legal theory, whether or not we have been informed
	of the possibility of such damage, and even if a remedy set forth
	herein is found to have failed of its essential purpose.

## Disclaimer

Your use of the Service is at your sole risk. The Service is
provided on an "AS IS" and "AS AVAILABLE" basis. The Service is
provided without warranties of any kind, whether express or implied,
including, but not limited to, implied warranties of merchantability,
fitness for a particular purpose, non-infringement or course of
performance.

gamefiar, LLC its subsidiaries, affiliates, and its licensors do not
warrant that

{type="a"}
1.  the Service will function uninterrupted, secure or available
	at any particular time or location;
2.  any errors or defects will be corrected;
3.  the Service is free of viruses or other harmful components; or
4.  the results of using the Service will meet your requirements.

## Exclusions

Some jurisdictions do not allow the exclusion of certain warranties
or the exclusion or limitation of liability for consequential or
incidental damages, so the limitations above may not apply to you.

## Governing Law
These Terms shall be governed and construed in accordance with the
laws of the United States, without regard to its conflict of law
provisions.

Our failure to enforce any right or provision of these Terms will
not be considered a waiver of those rights. If any provision of these
Terms is held to be invalid or unenforceable by a court, the remaining
provisions of these Terms will remain in effect. These Terms
constitute the entire agreement between us regarding our Service, and
supersede and replace any prior agreements we might have had between
us regarding the Service.

## Changes

We reserve the right, at our sole discretion, to modify or replace
these Terms at any time. If a revision is material we will provide at
least 30 days notice prior to any new terms taking effect. What
constitutes a material change will be determined at our sole
discretion.

By continuing to access or use our Service after any revisions
become effective, you agree to be bound by the revised terms. If you
do not agree to the new terms, you are no longer authorized to use the
Service.

# Contact Us

If you have any questions about these Terms, please contact us.
